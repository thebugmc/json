rootProject.name = "json"

pluginManagement {
    repositories {
        gradlePluginPortal()
        mavenLocal()
    }
}